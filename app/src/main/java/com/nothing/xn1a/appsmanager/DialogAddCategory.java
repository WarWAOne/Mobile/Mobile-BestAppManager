package com.nothing.xn1a.appsmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Created by Xakbash on 15/08/2016.
 */
public class DialogAddCategory {

    private Context mContext;
    private TagAdapter mAdapter;
    private Tag mTag;
    private int mIndex;
    private AlertDialog mBuilder;

    private EditText edt_name;
    //private String color;

    // TODO : Add Color Chooser
    public DialogAddCategory(Context context, TagAdapter adapter) {

        mContext = context;
        mAdapter = adapter;

        mBuilder = new AlertDialog.Builder(mContext)
                .setPositiveButton(R.string.btn_add, btn_add_click_lst)
                .setNegativeButton(R.string.btn_cancel, null)
                .create();

        edt_name = new EditText(mBuilder.getContext());
        edt_name.setSingleLine();
        edt_name.setHint("Name...");

        FrameLayout container = new FrameLayout(mContext);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(110, 6, 110, 6);

        edt_name.setLayoutParams(params);
        container.addView(edt_name);

        mBuilder.setView(container);
        mBuilder.setTitle(R.string.title_add_category);
    }

    public DialogAddCategory(Context context, Tag tag, int index) {

        mContext = context;
        mTag = tag;

        mBuilder = new AlertDialog.Builder(mContext)
                .setPositiveButton(R.string.btn_save, btn_add_click_lst)
                .setNegativeButton(R.string.btn_cancel, null)
                .create();

        edt_name = new EditText(mBuilder.getContext());
        edt_name.setSingleLine();
        edt_name.setHint("new name...");
        edt_name.setText(mTag.getName());

        FrameLayout container = new FrameLayout(mContext);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(110, 6, 110, 6);

        edt_name.setLayoutParams(params);
        container.addView(edt_name);

        mBuilder.setView(container);
        mBuilder.setTitle(R.string.title_edit_category);
    }

    private DialogInterface.OnClickListener btn_add_click_lst = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            MainActivity activity = (MainActivity) mContext;
            if (!edt_name.getText().toString().equals("")) {
                if (mTag == null) {
                    Tag tag = new Tag(edt_name.getText().toString());
                    TagTable tagTable = new TagTable(mContext);

                    tagTable.open();
                    int id = tagTable.create(tag);
                    tagTable.close();

                    mAdapter.add(tag);

                    /*mTabLayout.notifyAll();
                    mSectionsPagerAdapter.notifyAll();
                    mViewPager.notifyAll();*/
                }
                else {
                    if (edt_name.getText().toString() != String.valueOf(mTag.getName().toString())) {
                        mTag.setName(edt_name.getText().toString());
                        TagTable tagTable = new TagTable(mContext);
                        tagTable.open();
                        tagTable.edit(mTag);
                        tagTable.close();
                    }
                }
                dialogInterface.dismiss();
            }
            else {
                Toast.makeText(mContext, "Please enter a name", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public void show() {
        mBuilder.show();
    }
}
