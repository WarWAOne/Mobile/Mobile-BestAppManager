package com.nothing.xn1a.appsmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xakbash on 15/08/2016.
 */
public class DialogAddApp {

    private Context mContext;
    private AppAdapter mAdapter;
    private App mApp;
    private int mIndex;
    private AlertDialog mBuilder;

    private FrameLayout mFrameLayout;
    private LinearLayout mLinearLayout;

    private EditText edt_name;
    private CheckBox chb_platform;
    private AppCompatSpinner sp_licence;
    private ImageView icon;
    private ImageView tags;

    public DialogAddApp(Context context) {
        mContext = context;
        //mAdapter = adapter;

        mBuilder = new AlertDialog.Builder(mContext)
                .setPositiveButton(R.string.btn_add, btn_add_click_lst)
                .setNegativeButton(R.string.btn_cancel, null)
                .create();

        initDialog();

        mBuilder.setTitle(R.string.title_add_app);
    }

    public DialogAddApp(Context context, App app, int index) {

        mContext = context;
        //mAdapter = adapter;
        mApp = app;

        mBuilder = new AlertDialog.Builder(mContext)
                .setPositiveButton(R.string.btn_save, btn_add_click_lst)
                .setNegativeButton(R.string.btn_cancel, null)
                .create();

        initDialog();

        edt_name.setText(mApp.getName());
        //sp_category
        // sp_lisence
        mBuilder.setTitle(R.string.title_edit_app);
    }

    private void initDialog () {
        edt_name = new EditText(mBuilder.getContext());
        edt_name.setSingleLine();
        edt_name.setHint("Name...");

        tags = new ImageView(mBuilder.getContext());
        tags.setImageDrawable(mContext.getDrawable(R.drawable.ic_tag));
        tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO : Open dialog with list tags to coche and add text field
            }
        });

        icon = new ImageView(mBuilder.getContext());
        icon.setImageDrawable(mContext.getDrawable(R.drawable.ic_app)); // TODO : Change to Icon icon
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO : Brows file
            }
        });

        /*sp_category = new AppCompatSpinner(mBuilder.getContext());
        TagTable tagTable = new TagTable(mContext);
        tagTable.open();
        List<Tag> list = tagTable.findAll();
        List<String> categoriesTitles = new ArrayList<>();
        for (Tag tag : list) {
            categoriesTitles.add(tag.getName().toString());
        }
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, categoriesTitles);
        categoryAdapter.setDropDownViewResource(R.layout.item_spinner);
        sp_category.setAdapter(categoryAdapter);*/

        // Licence Spinner
        sp_licence = new AppCompatSpinner(mBuilder.getContext());
        String[] licencesTitles = mContext.getResources().getStringArray(R.array.lisences_titles);
        ArrayAdapter<String> licenceAdapter = new ArrayAdapter<>(mContext, R.layout.item_spinner, licencesTitles);
        licenceAdapter.setDropDownViewResource(R.layout.item_spinner);
        sp_licence.setAdapter(licenceAdapter);

        mFrameLayout = new FrameLayout(mBuilder.getContext());
        mLinearLayout = new LinearLayout(mBuilder.getContext());

        FrameLayout container = new FrameLayout(mContext);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(110, 6, 110, 6);
        layoutParams.height = 700;

        FrameLayout.LayoutParams edtParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        mFrameLayout.setLayoutParams(edtParams);
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mLinearLayout.setLayoutParams(layoutParams);

        mLinearLayout.addView(edt_name);
        mLinearLayout.addView(icon);
        mLinearLayout.addView(tags);
        mLinearLayout.addView(sp_licence);

        mLinearLayout.addView(mFrameLayout);

        container.addView(mLinearLayout);
        mBuilder.setView(container);
    }

    private DialogInterface.OnClickListener btn_add_click_lst = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            MainActivity activity = (MainActivity) mContext;
            if (!edt_name.getText().toString().equals("")) {
                if (mApp == null) {
                    App app = new App();
                    app.setName(edt_name.getText().toString());
                    app.setLicence(sp_licence.getSelectedItem().toString());

                    TagTable tagTable = new TagTable(mContext);
                    tagTable.open();
                    //Tag tag = tagTable.findByName(sp_category.getSelectedItem().toString());
                }
                else {
                    // TODO : si différents d'avant
                    mApp.setName(edt_name.getText().toString());
                    mApp.setLicence(sp_licence.getSelectedItem().toString());

                    TagTable tagTable = new TagTable(mContext);
                    tagTable.open();
                    //Tag tag = tagTable.findByName(sp_category.getSelectedItem().toString());
                }
                dialogInterface.dismiss();
            }
            else {
                Toast.makeText(mContext, "Please enter all fields", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public void show() {
        mBuilder.show();
    }
}
