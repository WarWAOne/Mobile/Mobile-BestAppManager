package com.nothing.xn1a.appsmanager;

/**
 * Created by xn1a on 14/02/17.
 */

public class AppTag {
    private int id;
    private int app_id;
    private int tag_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public int getTag_id() {
        return tag_id;
    }

    public void setTag_id(int tag_id) {
        this.tag_id = tag_id;
    }
}
