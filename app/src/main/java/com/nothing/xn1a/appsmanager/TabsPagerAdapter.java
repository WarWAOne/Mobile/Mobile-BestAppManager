package com.nothing.xn1a.appsmanager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xakbash on 20/04/2016.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        return mFragmentList.get(pos);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int pos) {
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        /*if (object instanceof MusicFragment) {
            return MainActivity.TAB_MUSIC;
        }
        else if (object instanceof PlayListListFragment) {
            return MainActivity.TAB_PLAYLISTS;
        }
        else if (object instanceof FolderFragment) {
            return MainActivity.TAB_FOLDERS;
        }
        else {*/
        return POSITION_NONE;
        //}
    }
}