package com.nothing.xn1a.appsmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class TagTable extends Database {

    public TagTable(Context pContext) {
        super(pContext);
    }

    public int create(Tag tag) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.TAG_NAME, tag.getName());
        int id = (int) mDb.insert(DatabaseHandler.TAG_TABLE_NAME, null, values);
        return id;
    }

    public void delete(int id) {
        String where = DatabaseHandler.TAG_KEY  + " = ?";
        String[] args = new String[]{String.valueOf(id)};
        mDb.delete(DatabaseHandler.TAG_TABLE_NAME, where, args);
    }

    public void edit(Tag tag) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.TAG_NAME, tag.getName());
        String where = DatabaseHandler.TAG_KEY  + "=?";
        String[] args = new String[]{String.valueOf(tag.getId())};
         mDb.update(DatabaseHandler.TAG_TABLE_NAME, values, where, args);
    }

    public List<Tag> findAll() {
        List<Tag> list = new ArrayList<Tag>();
        String selectQuery = "SELECT * FROM " + DatabaseHandler.TAG_TABLE_NAME;
        Cursor c = mDb.rawQuery(selectQuery, null);
        return getTagList(c);
    }

    public Tag findByName (String name) {
        String selectQuery = "SELECT * FROM " + DatabaseHandler.TAG_TABLE_NAME
                + " WHERE " + DatabaseHandler.TAG_NAME + " = ?";
        String[] args = new String[]{String.valueOf(name)};
        Cursor c = mDb.rawQuery(selectQuery, args);
        List<Tag> list = getTagList(c);
        return list.get(0);
    }

    public Tag find (int id) {
        String selectQuery = "SELECT * FROM " + DatabaseHandler.TAG_TABLE_NAME
                + " WHERE " + DatabaseHandler.TAG_KEY + " = ?";
        String[] args = new String[]{String.valueOf(id)};
        Cursor c = mDb.rawQuery(selectQuery, args);
        List<Tag> list = getTagList(c);
        return list.get(0);
    }

    private List<Tag> getTagList (Cursor c) {
        List<Tag> list = new ArrayList<>();
        if(c.getCount() == 0) {
            return list;
        }

        if (c.moveToFirst()) {
            do {
                Tag tag = new Tag();
                tag.setId(c.getInt((c.getColumnIndex(DatabaseHandler.TAG_KEY))));
                tag.setName(c.getString((c.getColumnIndex(DatabaseHandler.TAG_NAME))));
                list.add(tag);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }
}

