package com.nothing.xn1a.appsmanager;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class AppFragment extends Fragment {

    public static final String TAG = "AppFragment";
    public static final String ARG_CATEGORY_ID = "category_id";
    public static final int NO_CATEGORY_ID = -1;

    private View mView;
    private List<App> mList;
    String mFragment;
    private AppAdapter mAdapter;
    private MainActivity mActivity;

    private int mArgCategoryId;


    public AppFragment () {
    }

    public static AppFragment newInstance(int categoryId) {
        AppFragment fragment = new AppFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (MainActivity) getActivity();

        // Arguments
        if (getArguments() != null) {
            mArgCategoryId = getArguments().getInt(ARG_CATEGORY_ID);
        }

        AppTable appTable = new AppTable(getActivity());
        appTable.open();
        TagTable tagTable = new TagTable(getActivity());
        tagTable.open();

        if (mArgCategoryId == NO_CATEGORY_ID) {
            mList = appTable.findAll();
        }
        else {
            mList = appTable.findByTag(mArgCategoryId);
        }

        mAdapter = new AppAdapter(mActivity, mList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_app, container, false);

        //mAdapter.setOnItemClickListener(this);

        RecyclerView recyclerView = (RecyclerView) mView.findViewById(android.R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return mView;
    }
}