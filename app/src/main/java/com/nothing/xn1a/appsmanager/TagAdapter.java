package com.nothing.xn1a.appsmanager;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import java.util.List;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> implements View.OnClickListener {

    private List<Tag> mList;
    private MainActivity mContext;
    private LinearLayout mContainer;
    private OnItemClickListener mOnItemClickListener;
    private int mArgIsEdit;

    public TagAdapter(MainActivity context, List<Tag> items, int argIsEdit)
    {
        mList = items;
        mContext = context;
        mArgIsEdit = argIsEdit;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = (TagAdapter.OnItemClickListener) onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Tag tag);
    }

    public Object getItem(int position) {
        Tag item = mList.get(position);
        return item;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent,
                false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Tag item = mList.get(position);
        holder.item = item;
        holder.index = position;
        holder.name.setText(item.getName());

        holder.itemView.setTag(item);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Confirmation")
                        .setMessage("Are you sure you want to delete the tag " + item.getName().toString() + " ?")
                        .setIcon(R.drawable.ic_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // TODO : Delete Tag
                                TagTable tagTable = new TagTable(mContext);
                                tagTable.open();
                                tagTable.delete(item.getId());
                                tagTable.close();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAddCategory dialog = new DialogAddCategory(mContext, item, position);
                dialog.show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onClick(View view) {

    }

    // VIEW HOLDER
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView delete;
        ImageView edit;
        Tag item;
        int index;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
        }
    }

    public void add (Tag item) {
        mList.add(item);
        notifyDataSetChanged();
    }

    public void addToPosition (Tag item, int index) {
        mList.add(index, item);
        notifyDataSetChanged();
    }

    public void remove (Tag item) {
        mList.remove(item);
        notifyDataSetChanged();
    }
}

