package com.nothing.xn1a.appsmanager;

import android.support.v7.widget.RecyclerView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.ViewHolder> implements View.OnClickListener {

    private List<App> mList;
    private MainActivity mContext;
    private LinearLayout mContainer;
    private AdapterView.OnItemClickListener mOnItemClickListener;

    public AppAdapter(MainActivity context, List<App> items)
    {
        this.mList = items;
        mContext = context;
    }

    public void setOnItemClickListener(TagAdapter.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = (AdapterView.OnItemClickListener) onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, App app);
    }

    public Object getItem(int position) {
        App app = mList.get(position);
        return app;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = parent.inflate(mContext, R.layout.item_app, parent);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final App app = mList.get(position);

        holder.name.setText(app.getName());
        holder.description.setText(app.getDescription());
        holder.license.setText(app.getLicence());

        TagTable tagTable = new TagTable(mContext);

       // holder.tag.setText(tag.getName());

        // Get platforms
        /*StringBuffer strb_tags;
        strb_tags = getTags(position);

        holder.tags.setText(strb_tags);

        holder.itemView.setTag(bookmark);*/

           /* holder.popupMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenuBookmark popupMenu = new PopupMenuBookmark(mActivity, view, BookmarkAdapter.this, bookmark);
                    popupMenu.show();
                }
            });*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onClick(View view) {

    }

    // VIEW HOLDER
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        TextView category;
        TextView license;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
            category = (TextView) itemView.findViewById(R.id.tag);
            license = (TextView) itemView.findViewById(R.id.licence);
        }
    }

    public void add (App app) {
        mList.add(app);
        notifyDataSetChanged();
    }

    public void addToPosition (App app, int index) {
        mList.add(index, app);
        notifyDataSetChanged();
    }

    public void remove (App app) {
        mList.remove(app);
        notifyDataSetChanged();
    }
}

