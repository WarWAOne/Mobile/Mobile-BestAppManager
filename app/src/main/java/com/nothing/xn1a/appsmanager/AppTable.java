package com.nothing.xn1a.appsmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class AppTable extends Database {

    public AppTable(Context pContext) {
        super(pContext);
    }

    public int create(App app) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.APP_NAME, app.getName());
        values.put(DatabaseHandler.APP_DESCRIPTION, app.getDescription());
        values.put(DatabaseHandler.APP_LICENCE, app.getLicence());
        values.put(DatabaseHandler.APP_PLATFORMS, app.getPlatforms());
        values.put(DatabaseHandler.APP_ICON, app.getIcon_url());
        int id = (int) mDb.insert(DatabaseHandler.APP_TABLE_NAME, null, values);
        return id;
    }

    public void delete(int id) {
        String where = DatabaseHandler.APP_KEY  + " = ?";
        String[] args = new String[]{String.valueOf(id)};
        mDb.delete(DatabaseHandler.APP_TABLE_NAME, where, args);
    }

    public void edit(App app) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.APP_TABLE_NAME, app.getName());
        values.put(DatabaseHandler.APP_DESCRIPTION, app.getDescription());
        values.put(DatabaseHandler.APP_LICENCE, app.getLicence());
        values.put(DatabaseHandler.APP_PLATFORMS, app.getPlatforms());
        values.put(DatabaseHandler.APP_ICON, app.getIcon_url());

        String where = DatabaseHandler.APP_KEY  + " = ?";
        String[] args = new String[]{String.valueOf(app.getId())};

        mDb.update(DatabaseHandler.APP_TABLE_NAME, values, where, args);
    }

    public List<App> findByTag (int tagId) {
        String selectQuery =
                "SELECT * FROM " + DatabaseHandler.APP_TABLE_NAME
                + " LEFT JOIN " + DatabaseHandler.APPTAG_TABLE_NAME
                + " ON " + DatabaseHandler.APP_KEY + "=" + DatabaseHandler.APPTAG_APP_ID
                + " LEFT JOIN " + DatabaseHandler.TAG_TABLE_NAME + " ON " + DatabaseHandler.APPTAG_TAG_ID
                + "=?";
        String[] args = new String[]{String.valueOf(tagId)};
        Cursor c = mDb.rawQuery(selectQuery, args);
        return getAppList(c);
    }

    public List<App> findAll() {
        String selectQuery = "SELECT * FROM " + DatabaseHandler.APP_TABLE_NAME;
        Cursor c = mDb.rawQuery(selectQuery, null);
        return getAppList(c);
    }

    private List<App> getAppList (Cursor c) {
        List<App> list = new ArrayList<>();
        if(c.getCount() == 0) {
            return list;
        }

        if (c.moveToFirst()) {
            do {
                App app = new App();
                app.setId(c.getInt((c.getColumnIndex(DatabaseHandler.APP_KEY))));
                app.setName(c.getString((c.getColumnIndex(DatabaseHandler.APP_NAME))));
                app.setDescription(c.getString((c.getColumnIndex(DatabaseHandler.APP_DESCRIPTION))));
                app.setLicence(c.getString((c.getColumnIndex(DatabaseHandler.APP_LICENCE))));
                app.setPlatforms(c.getString((c.getColumnIndex(DatabaseHandler.APP_PLATFORMS))));
                app.setIcon_url(c.getInt((c.getColumnIndex(DatabaseHandler.APP_ICON))));
                list.add(app);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }
}

