package com.nothing.xn1a.appsmanager;

/**
 * Created by Xakbash on 15/08/2016.
 */
public class Tag {

    private int id;
    private String name;
    private String color;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public Tag(int id, String name, String color) {
        this.name = name;
        this.color = color;
        this.id = id;
    }

    public Tag(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
