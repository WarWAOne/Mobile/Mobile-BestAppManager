package com.nothing.xn1a.appsmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Xakbash on 26/12/2014.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // TABLE APP
    public static final String APP_TABLE_NAME = "App";
    public static final String APP_KEY = "id";
    public static final String APP_NAME = "name";
    public static final String APP_DESCRIPTION = "description";
    public static final String APP_LICENCE = "licence";
    public static final String APP_PLATFORMS = "platforms";
    public static final String APP_ICON = "icon";

    // TABLE TAG
    public static final String TAG_TABLE_NAME = "Tag";
    public static final String TAG_KEY = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_COLOR = "color";

    // TABLE APP-TAG
    public static final String APPTAG_TABLE_NAME = "AppTag";
    public static final String APPTAG_KEY = "id";
    public static final String APPTAG_TAG_ID = "tag_id";
    public static final String APPTAG_APP_ID = "app_id";

    public static final String APPTAG_TABLE_CREATE =
            "CREATE TABLE " +
                    APPTAG_TABLE_NAME + "("  +
                    APPTAG_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    APPTAG_APP_ID + " INTEGER, " +
                    APPTAG_TAG_ID + " INTEGER)";

    public static final String APP_TABLE_CREATE =
            "CREATE TABLE " +
                    APP_TABLE_NAME + "(" +
                    APP_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    APP_NAME + " TEXT, " +
                    APP_DESCRIPTION + " TEXT, " +
                    APP_LICENCE + " TEXT, " +
                    APP_PLATFORMS + " TEXT, " +
                    APP_ICON + " INTEGER)";

    public static final String TAG_TABLE_CREATE =
            "CREATE TABLE " +
                    TAG_TABLE_NAME + "(" +
                    TAG_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TAG_COLOR + " TEXT" +
                    TAG_NAME + " TEXT)";

    public static final String APP_TABLE_DROP = "DROP TABLE IF EXISTS " + APP_TABLE_NAME + ";";
    public static final String TAG_TABLE_DROP = "DROP TABLE IF EXISTS " + TAG_TABLE_NAME + ";";
    public static final String APPTAG_TABLE_DROP = "DROP TABLE IF EXISTS " + APPTAG_TABLE_NAME + ";";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(APP_TABLE_CREATE);
        db.execSQL(TAG_TABLE_CREATE);
        db.execSQL(APPTAG_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(APP_TABLE_DROP);
        db.execSQL(TAG_TABLE_DROP);
        db.execSQL(APPTAG_TABLE_DROP);
        onCreate(db);
    }
}
