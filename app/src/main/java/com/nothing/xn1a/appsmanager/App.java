package com.nothing.xn1a.appsmanager;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class App {
    private int id;
    private String name;
    private String description;
    private String platforms;
    private String licence;
    private int icon_url;

    public App () {
    }

    public App(String name, String description, String platforms, String licence, int icon_url) {
        this.name = name;
        this.description = description;
        this.platforms = platforms;
        this.licence = licence;
        this.icon_url = icon_url;
    }

    public App(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public int getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(int icon_url) {
        this.icon_url = icon_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlatforms() {
        return platforms;
    }

    public void setPlatforms(String platforms) {
        this.platforms = platforms;
    }

}
