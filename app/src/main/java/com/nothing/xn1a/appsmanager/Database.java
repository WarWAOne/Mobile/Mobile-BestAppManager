package com.nothing.xn1a.appsmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class Database {
    protected final static int VERSION = 1;
    protected final static String NAME = "AppManager.db";

    protected SQLiteDatabase mDb;
    protected DatabaseHandler mHandler;

    public Database(Context pContext) {
        this.mHandler = new DatabaseHandler(pContext, NAME, null, VERSION);
    }

    public SQLiteDatabase open() {
        mDb = mHandler.getWritableDatabase();
        return mDb;
    }

    public void close() {
        mDb.close();
    }

    public SQLiteDatabase getDb() {
        return mDb;
    }
}