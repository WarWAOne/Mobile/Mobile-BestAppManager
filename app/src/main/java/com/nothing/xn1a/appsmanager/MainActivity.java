package com.nothing.xn1a.appsmanager;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    public static final int TAB_APPS = 0;
    public static final int TAB_TAGS = 1;

    private CharSequence mTitle;

    public TabsPagerAdapter mSectionsPagerAdapter;
    public ViewPager mViewPager;
    private Toolbar mToolbar;
    private Toolbar mBotToolbar;
    public FloatingActionButton mFab;
    private String mFragment;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBotToolbar = new Toolbar(this);
        mBotToolbar = (Toolbar) findViewById(R.id.toolbar_bot);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mFab.setImageResource(R.drawable.ic_bookmark_plus_outline);
                String[] tab_titles = getResources().getStringArray(R.array.tab_titles);
                getSupportActionBar().setDisplayShowTitleEnabled(true);
                setTitle(tab_titles[position]);
                if (position == TAB_APPS)
                {
                    mFab.setImageResource(R.drawable.ic_plus_box);
                }
                else
                {
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        setupViewPager(mViewPager);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);

        TypedArray tab_icons = getResources().obtainTypedArray(R.array.tab_icons);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.getTabAt(TAB_APPS).setIcon(tab_icons.getResourceId(TAB_APPS, -1));
        mTabLayout.getTabAt(TAB_TAGS).setIcon(tab_icons.getResourceId(TAB_TAGS, -1));

        // Fragments
        if (savedInstanceState != null) {
            mFragment = savedInstanceState.getString("fragment");
        }
        else {
            mFragment = getIntent().getStringExtra("fragment");
        }

        mViewPager.setCurrentItem(TAB_APPS);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mViewPager.getCurrentItem() == TAB_APPS) {
                    DialogAddApp dialog = new DialogAddApp(MainActivity.this);
                    dialog.show();
                }
                else {
                    TagFragment frag = (TagFragment) mSectionsPagerAdapter.getItem(TAB_TAGS);
                    frag.showDialogAddCategory(MainActivity.this);
                }
                // TODO : Si Apps -> Add App Dialog
                // TODO : Si Categories -> Add Tag Dialog
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        mSectionsPagerAdapter= new TabsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFrag(AppFragment.newInstance(AppFragment.NO_CATEGORY_ID), "Apps");
        mSectionsPagerAdapter.addFrag(TagFragment.newInstance(TagFragment.EDIT_FALSE), "Categories");
        viewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        mToolbar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
