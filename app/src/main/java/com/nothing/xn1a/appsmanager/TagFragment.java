package com.nothing.xn1a.appsmanager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

/**
 * Created by Xakbash on 25/10/2016.
 */

public class TagFragment extends Fragment {

    public static final String TAG = "TagFragment";
    public static final String ARG_IS_EDIT = "edit";
    public static final int EDIT_TRUE = 1;
    public static final int EDIT_FALSE = 0;

    private View mView;
    private List<Tag> mList;
    String mFragment;
    private TagAdapter mAdapter;
    private MainActivity mActivity;

    private int mArgIsEdit;

    public TagFragment() {
    }

    public static TagFragment newInstance(int isEdit) {
        TagFragment fragment = new TagFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    public void showDialogAddCategory (Context context) {
        DialogAddCategory dialog = new DialogAddCategory(context, mAdapter);
        dialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (MainActivity) getActivity();

        // Arguments
        if (getArguments() != null) {
            mArgIsEdit = getArguments().getInt(ARG_IS_EDIT);
        }

        TagTable tagTable = new TagTable(getActivity());
        tagTable.open();
        mList = tagTable.findAll();
        tagTable.close();
        mAdapter = new TagAdapter(mActivity, mList, mArgIsEdit);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_category, container, false);

       // mAdapter.setOnItemClickListener(this);

        RecyclerView recyclerView = (RecyclerView) mView.findViewById(android.R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return mView;
    }
}